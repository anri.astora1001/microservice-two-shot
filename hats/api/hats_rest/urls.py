from django.urls import path
from .views import *

urlpatterns = [
    path("hats/", api_hats, name="api_hats"),
]